# CodeIgniter AppStarter + Lando

## Serve localy
```sh 
lando start
lando db-import "/.bin/db/dump.sql.gz"

cd docroot
composer i
```

See website at [codeigniter-appstarter.lndo.site](https://codeigniter-appstarter.lndo.site/ "Edit this in lando.yml")

### Save to database
```sh
lando db-export "/.bin/db/dump.sql"
```